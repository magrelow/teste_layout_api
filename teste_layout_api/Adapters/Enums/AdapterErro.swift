//
//  AdapterErro.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

public enum AdapterErro {
    case genericError
    case imageNilError
    case imageDownloadError
    case noError
}
