//
//  ApiGetAdapterProtocol.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

protocol ApiGetAdapterProtocol {
    func getSimpleApi(url: String, completionHandler: @escaping(Data?, AdapterErro) -> Void)
}
