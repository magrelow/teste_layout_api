//
//  ProductCollectionViewCell.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productsImageView: UIImageView!
    @IBOutlet weak var productsContentView: UIView!
}
