//
//  ExtensionsUtil.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    public func getImageFromURL(urlString:String) -> UIImage {
        let url = URL(string: urlString)
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            return UIImage(data: imageData)!
        } else {
            return UIImage()
        }
    }
}

extension UIView {

    func dropShadow(scale: Bool = true) {
      layer.masksToBounds = false
      layer.shadowColor = UIColor.black.cgColor
      layer.shadowOpacity = 0.1
      layer.shadowOffset = CGSize(width: -1, height: 1)
      //layer.shadowRadius = 0.1
      //layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      //layer.shouldRasterize = true
      //layer.rasterizationScale = UIScreen.main.scale
    }

}
