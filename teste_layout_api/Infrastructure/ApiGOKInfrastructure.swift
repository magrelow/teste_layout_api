//
//  ApiGOKInfrastructure.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

class ApiGOKInfrastructure: ApiGOKInfrastructureProtocol {
    
    var apiGetAdapter: ApiGetAdapterProtocol = ApiGetAdapter()
    
    func getContentsFromGoK(completionHandler: @escaping(GokModelResponse?, InfraError) -> Void) {
        apiGetAdapter.getSimpleApi(url: "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products")
        { (resultData, error) -> Void in
            if error == .noError {
                let decoder = JSONDecoder()
                
                do {
                    let gitResponse = try decoder.decode(GokModelResponse.self, from: resultData!)
                    completionHandler(gitResponse, .noError)
                } catch {
                    completionHandler(nil, .convertionJsonError)
                }
                
            } else {
                completionHandler(nil, .genericError)
            }
        }
    }
}
