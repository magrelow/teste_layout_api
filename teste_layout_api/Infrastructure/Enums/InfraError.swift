//
//  InfraError.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

enum InfraError {
    case convertionJsonError
    case genericError
    case noError
}
