//
//  GokModelResponse.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

struct GokModelResponse: Decodable {
    var spotlight: [GoKModelSpotlightResponse]?
    var products: [GoKModelProductsResponse]?
    var cash: GoKModelCashResponse?
    
    enum CodingKeys: String, CodingKey {
        case spotlight
        case products
        case cash
    }
}

struct GoKModelSpotlightResponse: Decodable {
    var name: String?
    var bannerURL: String?
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case bannerURL
        case description
    }
}

struct GoKModelProductsResponse: Decodable {
    var name: String?
    var imageURL: String?
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case imageURL
        case description
    }
}

struct GoKModelCashResponse: Decodable {
    var title: String?
    var bannerURL: String?
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case bannerURL
        case description
    }
}
