//
//  ViewViewModel.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

struct ViewViewModel {
    var spotlight: [ViewViewModelSpotligh]?
    var products: [ViewViewModelProducts]?
    var cash: ViewViewModelCash?
    
    init(gokModelResponse: GokModelResponse?){
        self.spotlight = []
        self.products = []
        
        gokModelResponse?.spotlight!.forEach {
            self.spotlight?.append(ViewViewModelSpotligh(name: $0.name, bannerURL: $0.bannerURL, description: $0.description))
        }
        
        gokModelResponse?.products?.forEach {
            self.products?.append(ViewViewModelProducts(name: $0.name, imageURL: $0.imageURL, description: $0.description))
        }
        
        self.cash = ViewViewModelCash(title: gokModelResponse?.cash?.title, bannerURL: gokModelResponse?.cash?.bannerURL, description: gokModelResponse?.cash?.description)
        
    }
}

struct ViewViewModelSpotligh{
    var name: String?
    var bannerURL: String?
    var description: String?
    var image: UIImage? = UIImage()
    
    init(name: String?, bannerURL: String?, description: String?) {
        self.name = name
        self.bannerURL = bannerURL
        self.description = description
        self.image = image!.getImageFromURL(urlString: bannerURL!)
    }
}

struct ViewViewModelProducts{
    var name: String?
    var imageURL: String?
    var description: String?
    var image: UIImage? = UIImage()
    
    init(name: String?, imageURL: String?, description: String?) {
        self.name = name
        self.imageURL = imageURL
        self.description = description
        self.image = image!.getImageFromURL(urlString: imageURL!)
    }
}

struct ViewViewModelCash{
    var title: String?
    var bannerURL: String?
    var description: String?
    var image: UIImage? = UIImage()
    
    init(title: String?, bannerURL: String?, description: String?) {
        self.title = title
        self.bannerURL = bannerURL
        self.description = description
        self.image = image!.getImageFromURL(urlString: bannerURL!)
    }
}

