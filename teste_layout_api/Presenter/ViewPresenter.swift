//
//  ViewPresenter.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

class ViewPresenter: ViewPresenterProtocol {
    
    var apiGoKInfrastructure: ApiGOKInfrastructureProtocol = ApiGOKInfrastructure()
    
    func carregarDadosDeGoK(completionHandler: @escaping (ViewViewModel?, PresenterError) -> Void) {
        
        var resultViewModel: ViewViewModel?
        
        apiGoKInfrastructure.getContentsFromGoK() {
            (resultData, error) -> Void in

            if error == .noError {
                resultViewModel = ViewViewModel(gokModelResponse: resultData)
                completionHandler(resultViewModel, .noError)
            } else {
                completionHandler(resultViewModel, .genericError)
            }
        }
    }
}
