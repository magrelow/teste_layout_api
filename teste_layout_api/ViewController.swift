//
//  ViewController.swift
//  teste_layout_api
//
//  Created by Eric Soares Filho on 31/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var cashImageView: UIImageView!
    
    var viewModel: ViewViewModel?
    
    //Presenter
    var viewPresenter: ViewPresenterProtocol = ViewPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.initializer()
    }
    
    func initializer() {
        viewPresenter.carregarDadosDeGoK() { [weak self] (result, error) -> Void in
            if error == .noError {
                DispatchQueue.main.async {
                    self!.viewModel = result
                    self?.configureFront()
                }
            } else {
                print("error")
            }
        }
    }
    
    func configureFront() {
        self.cashImageView.image = viewModel?.cash?.image
      
        
        mainCollectionView.reloadData()
        productCollectionView.reloadData()
    }


}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if viewModel == nil {
            return 0
        } else {
            if collectionView == mainCollectionView {
                return (viewModel?.spotlight?.count)!
            } else {
                return (viewModel?.products?.count)!
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == mainCollectionView {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell",
                                                          for: indexPath) as! MainContentCollectionViewCell
            
            cell.mainImageView.image = viewModel?.spotlight![indexPath.row].image
            cell.mainImageView.dropShadow()
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell2",
                                                              for: indexPath) as! ProductCollectionViewCell
            
            cell.productsImageView.image = viewModel?.products![indexPath.row].image
            cell.productsContentView.dropShadow()
            return cell
            
        }
    }
    
}

